var widgetAPI = new Common.API.Widget();
var tvKey = new Common.API.TVKeyValue();
var current_item=0;
var total_item=2;
var direction;
var popExit=false;
var popQuit=false;
var Main =
{

};

Main.onLoad = function()
{
	// Enable key event processing
	if(sessionStorage.getItem("current")!=null)
		current_item=sessionStorage.getItem("current")-1;
	navigation("right");
	this.enableKeys();
	widgetAPI.sendReadyEvent();
};

Main.onUnload = function()
{

};

Main.enableKeys = function()
{
	document.getElementById("anchor").focus();
};

Main.keyDown = function()
{
	var keyCode = event.keyCode;
	alert("Key pressed: " + keyCode);

	switch(keyCode)
	{
		case tvKey.KEY_RETURN:
		case tvKey.KEY_PANEL_RETURN:
			alert("RETURN");
			widgetAPI.blockNavigation(event);
			if(popQuit==false)
				openQuitPopUp();
			else if(popQuit==true)
				closeQuitPopUp();
			if(popExit==true)
				closeQuitPopUp();
			inf=false;
			break;
		case tvKey.EXIT:
			//widgetAPI.blockNavigation(event);
			widgetAPI.preventDefault(); 
		//	openExitPopUp();
			break;
		case tvKey.KEY_LEFT:
			alert("LEFT");
			navigation("left");
			break;
		case tvKey.KEY_RIGHT:
			alert("RIGHT");
			navigation("right");
			break;
		case tvKey.KEY_UP:
			alert("UP");
			break;
		case tvKey.KEY_DOWN:
			alert("DOWN");
			break;
		case tvKey.KEY_ENTER:
		case tvKey.KEY_PANEL_ENTER:
			if(popQuit==true)
				widgetAPI.sendReturnEvent();
			else if(popExit==true)
				widgetAPI.sendExitEvent();
			else
				changePage();
			alert("ENTER");
			break;
		case tvKey.KEY_EXIT:
			widgetAPI.sendExitEvent();
			alert("EXIT");
			break;
		default:
			alert("Unhandled key");
			break;
	}
};

function changePage(){
	if(current_item==1)
		sessionStorage.setItem("language","En");
	 alert(sessionStorage.geItem);

	if(current_item==2)
		sessionStorage.setItem("language","Ar");
	 alert(sessionStorage.getItem);
	parent.location.replace("menu.html");
}

function navigation(direction){
	$("#fig"+current_item).removeClass("clicked");
	if(direction=="left"){
		if(current_item==1)
			current_item=total_item
			else
				current_item--;	
	}
	if(direction=="right"){
		if(current_item==total_item)
			current_item=1;
		else
			current_item++;
	}
	$("#fig"+current_item).addClass("clicked");
}
function openExitPopUp(){
	popExit=true;
	document.getElementById("popupQuit").style.backgroundImage = 'url("images/backgrounds/popupQuit.png")';
	element = document.getElementById("background");
	element.style.display = "block";
	element= document.getElementById("popupQuit");
	element.style.display = "block";
}

function openQuitPopUp(){
	popQuit=true;
	
	document.getElementById("popupQuit").style.backgroundImage = 'url("images/backgrounds/popupQuit.png")';
	element = document.getElementById("background");
	element.style.display = "block";
	element= document.getElementById("popupQuit");
	element.style.display = "block";
}
function closeQuitPopUp()
{
	popQuit=false;
	popExit=false;
	element = document.getElementById("background");
	element.style.display = "none";
	element = document.getElementById("popupQuit");
	element.style.display = "none";
}
window.onShow = function()
{ Connexion.init();
setInterval('Connexion.cyclicInternetConnectionCheck()', Connexion.internetConnectionInterval); 
}
