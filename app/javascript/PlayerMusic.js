var PlayerMusic =
{
    plugin : null,
    state : -1,
    skipState : -1,
    stopCallback : null,    /* Callback function to be set by client */
    originalSource : null,
    
    STOPPED : 0,
    PLAYING : 1,
    PAUSED : 2,  
    FORWARD : 3,
    REWIND : 4
}

PlayerMusic.init = function()
{
    var success = true;
          alert("success vale :  " + success);    
    this.state = this.STOPPED;
    
    this.plugin = document.getElementById("pluginPlayer");
    
    if (!this.plugin)
    {
          alert("success vale this.plugin :  " + success);    
         success = false;
    }
    
    alert("success vale :  " + success);    
    
    this.setWindow();
    
    alert("success vale :  " + success);    
    
    this.plugin.OnCurrentPlayTime = 'PlayerMusic.setCurTime';
    this.plugin.OnStreamInfoReady = 'PlayerMusic.setTotalTime';
    this.plugin.OnBufferingStart = 'PlayerMusic.onBufferingStart';
    this.plugin.OnBufferingProgress = 'PlayerMusic.onBufferingProgress';
    this.plugin.OnBufferingComplete = 'PlayerMusic.onBufferingComplete';           
            
    alert("success vale :  " + success);       
    return success;
}

PlayerMusic.deinit = function()
{
      alert("Player deinit !!! " );       
      
      if (this.plugin)
      {
            this.plugin.Stop();
      }
}

PlayerMusic.setWindow = function()
{
    this.plugin.SetDisplayArea(403, 120, 475, 276);
}

PlayerMusic.setFullscreen = function()
{
    this.plugin.SetDisplayArea(0, 0, 960, 540);
}

PlayerMusic.setVideoURL = function(url)
{
    this.url = url;
    alert("URL = " + this.url);
}

PlayerMusic.playVideo = function()
{
    if (this.url == null)
    {
        alert("No videos to play");
    }
    else
    {
        this.state = this.PLAYING;
        document.getElementById("play").style.opacity = '0.2';
        document.getElementById("stop").style.opacity = '1.0';
        document.getElementById("pause").style.opacity = '1.0';
        document.getElementById("forward").style.opacity = '1.0';
        document.getElementById("rewind").style.opacity = '1.0';
        DisplayMusic.status("Play");
        this.setWindow();
        this.plugin.Play( this.url );
        AudioMusic.plugin.SetSystemMute(false); 
    }
}

PlayerMusic.pauseVideo = function()
{
    this.state = this.PAUSED;
    document.getElementById("play").style.opacity = '1.0';
    document.getElementById("stop").style.opacity = '1.0';
    document.getElementById("pause").style.opacity = '0.2';
    document.getElementById("forward").style.opacity = '0.2';
    document.getElementById("rewind").style.opacity = '0.2';
    DisplayMusic.status("Pause");
    this.plugin.Pause();
}

PlayerMusic.stopVideo = function()
{
    if (this.state != this.STOPPED)
    {
        this.state = this.STOPPED;
        document.getElementById("play").style.opacity = '1.0';
        document.getElementById("stop").style.opacity = '0.2';
        document.getElementById("pause").style.opacity = '0.2';
        document.getElementById("forward").style.opacity = '0.2';
        document.getElementById("rewind").style.opacity = '0.2';
        DisplayMusic.status("Stop");
        this.plugin.Stop();
        DisplayMusic.setTime(0);
        
        if (this.stopCallback)
        {
            this.stopCallback();
        }
    }
    else
    {
        alert("Ignoring stop request, not in correct state");
    }
}

PlayerMusic.resumeVideo = function()
{
    this.state = this.PLAYING;
    document.getElementById("play").style.opacity = '0.2';
    document.getElementById("stop").style.opacity = '1.0';
    document.getElementById("pause").style.opacity = '1.0';
    document.getElementById("forward").style.opacity = '1.0';
    document.getElementById("rewind").style.opacity = '1.0';
    DisplayMusic.status("Play");
    this.plugin.Resume();
}

PlayerMusic.skipForwardVideo = function()
{
    this.skipState = this.FORWARD;
    this.plugin.JumpForward(5);    
}

PlayerMusic.skipBackwardVideo = function()
{
    this.skipState = this.REWIND;
    this.plugin.JumpBackward(5);
}

PlayerMusic.getState = function()
{
    return this.state;
}

// Global functions called directly by the player 

PlayerMusic.onBufferingStart = function()
{
	DisplayMusic.status("Buffering...");
    switch(this.skipState)
    {
        case this.FORWARD:
            document.getElementById("forward").style.opacity = '0.2';
            break;
        
        case this.REWIND:
            document.getElementById("rewind").style.opacity = '0.2';
            break;
    }
}

PlayerMusic.onBufferingProgress = function(percent)
{
	DisplayMusic.status("Buffering:" + percent + "%");
}

PlayerMusic.onBufferingComplete = function()
{
	DisplayMusic.status("Play");
    switch(this.skipState)
    {
        case this.FORWARD:
            document.getElementById("forward").style.opacity = '1.0';
            break;
        
        case this.REWIND:
            document.getElementById("rewind").style.opacity = '1.0';
            break;
    }
}

PlayerMusic.setCurTime = function(time)
{
	DisplayMusic.setTime(time);
}

PlayerMusic.setTotalTime = function()
{
	DisplayMusic.setTotalTime(PlayerMusic.plugin.GetDuration());
}

onServerError = function()
{
	DisplayMusic.status("Server Error!");
}

OnNetworkDisconnected = function()
{
	DisplayMusic.status("Network Error!");
}

getBandwidth = function(bandwidth) { alert("getBandwidth " + bandwidth); }

onDecoderReady = function() { alert("onDecoderReady"); }

onRenderError = function() { alert("onRenderError"); }

stopPlayer = function()
{
	PlayerMusic.stopVideo();
}

setTottalBuffer = function(buffer) { alert("setTottalBuffer " + buffer); }

setCurBuffer = function(buffer) { alert("setCurBuffer " + buffer); }
