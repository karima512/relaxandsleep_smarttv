var widgetAPI = new Common.API.Widget();
var tvKey = new Common.API.TVKeyValue();
var language;
var Music = {
	selectedVideo : 0,
	mode : 0,
	mute : 0,

	UP : 0,
	DOWN : 1,

	WINDOW : 0,
	FULLSCREEN : 1,

	NMUTE : 0,
	YMUTE : 1
}

Music.onLoad = function() {
	if (PlayerMusic.init() && AudioMusic.init() && DisplayMusic.init()) {
		if (sessionStorage.getItem("language") != null)
			language = sessionStorage.getItem("language");
		alert(language);
		if (language == "En") {
			if (ServerAudio.init()) {
				DisplayMusic.setVolume(AudioMusic.getVolume());
				DisplayMusic.setTime(0);

				PlayerMusic.stopCallback = function() {
					/*
					 * Return to windowed mode when video is stopped (by choice
					 * or when it reaches the end)
					 */
					Music.setWindowMode();
				}

				// Start retrieving data from server
				ServerAudio.dataReceivedCallback = function() {
					/* Use video information when it has arrived */
					DisplayMusic.setVideoList(Data.getVideoNames());
					Music.updateCurrentVideo();
				}
				ServerAudio.fetchVideoList(); /*
												 * Request video information
												 * from server
												 */

				// Enable key event processing
				this.enableKeys();

				widgetAPI.sendReadyEvent();
			} else {
				alert("Failed to initialise");
			}

		} else if (language == "Ar") {
			alert(language);
			if (ServerAudioArabe.init())
			{
				DisplayMusic.setVolume(AudioMusic.getVolume());
				DisplayMusic.setTime(0);

				PlayerMusic.stopCallback = function() {
					/*
					 * Return to windowed mode when video is stopped (by choice
					 * or when it reaches the end)
					 */
					Music.setWindowMode();
				}

				// Start retrieving data from server
				ServerAudioArabe.dataReceivedCallback = function() {
					/* Use video information when it has arrived */
					DisplayMusic.setVideoList(Data.getVideoNames());
					Music.updateCurrentVideo();
				}
				ServerAudioArabe.fetchVideoList(); /*
													 * Request video information
													 * from server
													 */

				// Enable key event processing
				this.enableKeys();

				widgetAPI.sendReadyEvent();
			} else {
				alert("Failed to initialise");
			}
		}
	}
}

Music.onUnload = function() {
	PlayerMusic.deinit();
}

Music.updateCurrentVideo = function(move) {
	PlayerMusic.setVideoURL(Data.getVideoURL(this.selectedVideo));

	DisplayMusic.setVideoListPosition(this.selectedVideo, move);

	DisplayMusic.setDescription(Data.getVideoDescription(this.selectedVideo));
}

Music.enableKeys = function() {
	document.getElementById("anchor").focus();
}

Music.keyDown = function() {
	var keyCode = event.keyCode;
	alert("Key pressed: " + keyCode);

	switch (keyCode) {
	case tvKey.KEY_RETURN:
	case tvKey.KEY_PANEL_RETURN:

		alert("RETURN");
		PlayerMusic.stopVideo();
	//	widgetAPI.sendReturnEvent();
		widgetAPI.blockNavigation(event);
		parent.location.replace("menu.html");
		break;
	break;

case tvKey.KEY_PLAY:
	alert("PLAY");

	this.handlePlayKey();
	break;

case tvKey.KEY_STOP:
	alert("STOP");
	PlayerMusic.stopVideo();
	break;

case tvKey.KEY_PAUSE:
	alert("PAUSE");
	this.handlePauseKey();
	break;

case tvKey.KEY_FF:
	alert("FF");
	if (PlayerMusic.getState() != PlayerMusic.PAUSED)
		PlayerMusic.skipForwardVideo();
	break;

case tvKey.KEY_RW:
	alert("RW");
	if (PlayerMusic.getState() != PlayerMusic.PAUSED)
		PlayerMusic.skipBackwardVideo();
	break;

case tvKey.KEY_VOL_UP:
case tvKey.KEY_PANEL_VOL_UP:
	alert("VOL_UP");
	if (this.mute == 0)
		AudioMusic.setRelativeVolume(0);
	break;

case tvKey.KEY_VOL_DOWN:
case tvKey.KEY_PANEL_VOL_DOWN:
	alert("VOL_DOWN");
	if (this.mute == 0)
		AudioMusic.setRelativeVolume(1);
	break;

case tvKey.KEY_DOWN:
	alert("DOWN");
	this.selectNextVideo(this.DOWN);
	break;

case tvKey.KEY_UP:
	alert("UP");
	this.selectPreviousVideo(this.UP);
	break;

case tvKey.KEY_ENTER:
case tvKey.KEY_PANEL_ENTER:
	alert("ENTER");
	// this.toggleMode();
	break;

case tvKey.KEY_MUTE:
	alert("MUTE");
	this.muteMode();
	break;

default:
	alert("Unhandled key");
	break;
}
}

Music.handlePlayKey = function() {
	switch (PlayerMusic.getState()) {
	case PlayerMusic.STOPPED:
		PlayerMusic.playVideo();
		break;

	case PlayerMusic.PAUSED:
		PlayerMusic.resumeVideo();
		break;

	default:
		alert("Ignoring play key, not in correct state");
		break;
	}
}

Music.handlePauseKey = function() {
	switch (PlayerMusic.getState()) {
	case PlayerMusic.PLAYING:
		PlayerMusic.pauseVideo();
		break;

	default:
		alert("Ignoring pause key, not in correct state");
		break;
	}
}

Music.selectNextVideo = function(down) {
	PlayerMusic.stopVideo();

	this.selectedVideo = (this.selectedVideo + 1) % Data.getVideoCount();

	this.updateCurrentVideo(down);
}

Music.selectPreviousVideo = function(up) {
	PlayerMusic.stopVideo();

	if (--this.selectedVideo < 0) {
		this.selectedVideo += Data.getVideoCount();
	}

	this.updateCurrentVideo(up);
}

Music.setFullScreenMode = function() {
	if (this.mode != this.FULLSCREEN) {
		DisplayMusic.hide();

		PlayerMusic.setFullscreen();

		this.mode = this.FULLSCREEN;
	}
}

Music.setWindowMode = function() {
	if (this.mode != this.WINDOW) {
		DisplayMusic.show();

		PlayerMusic.setWindow();

		this.mode = this.WINDOW;
	}
}

Music.toggleMode = function() {
	if (PlayerMusic.getState() == PlayerMusic.PAUSED) {
		PlayerMusic.resumeVideo();
	}
	switch (this.mode) {
	case this.WINDOW:
		this.setFullScreenMode();
		break;

	case this.FULLSCREEN:
		this.setWindowMode();
		break;

	default:
		alert("ERROR: unexpected mode in toggleMode");
		break;
	}
}

Music.setMuteMode = function() {
	if (this.mute != this.YMUTE) {
		var volumeElement = document.getElementById("volumeInfo");
		// Audio.plugin.SetSystemMute(true);
		AudioMusic.plugin.SetUserMute(true);
		document.getElementById("volumeBar").style.backgroundImage = "url(../../images/videoBox/muteBar.png)";
		document.getElementById("volumeIcon").style.backgroundImage = "url(../../images/videoBox/mute.png)";
		widgetAPI.putInnerHTML(volumeElement, "MUTE");
		this.mute = this.YMUTE;
	}
}

Music.noMuteMode = function() {
	if (this.mute != this.NMUTE) {
		AudioMusic.plugin.SetUserMute(false);
		document.getElementById("volumeBar").style.backgroundImage = "url(../../images/videoBox/volumeBar.png)";
		document.getElementById("volumeIcon").style.backgroundImage = "url(../../images/videoBox/volume.png)";
		DisplayMusic.setVolume(AudioMusic.getVolume());
		this.mute = this.NMUTE;
	}
}

Music.muteMode = function() {
	switch (this.mute) {
	case this.NMUTE:
		this.setMuteMode();
		break;

	case this.YMUTE:
		this.noMuteMode();
		break;

	default:
		alert("ERROR: unexpected mode in muteMode");
		break;
	}
}

window.onShow = function()
{ Connexion.init();
setInterval('Connexion.cyclicInternetConnectionCheck()', Connexion.internetConnectionInterval); 
};

