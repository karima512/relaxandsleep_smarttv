var widgetAPI = new Common.API.Widget();
var tvKey = new Common.API.TVKeyValue();
var language;
var Video =
{
    selectedVideo : 0,
    mode : 0,
    mute : 0,
    
    UP : 0,
    DOWN : 1,

    WINDOW : 0,
    FULLSCREEN : 1,
    
    NMUTE : 0,
    YMUTE : 1
}

Video.onLoad = function()
{
    if ( Player.init() && Audio.init() && Display.init())
    {
    	if(sessionStorage.getItem("language")!=null)
   		 language=sessionStorage.getItem("language");
   		 alert(language);
   		 if(language=="En") {
   			 if(Server.init())
   				 {
   				 Display.setVolume( Audio.getVolume() );
   		        Display.setTime(0);
   		        
   		        Player.stopCallback = function()
   		        {
   		            /* Return to windowed mode when video is stopped
   		                (by choice or when it reaches the end) */
   		        	Video.setWindowMode();
   		        }

   		        // Start retrieving data from server
   		        Server.dataReceivedCallback = function()
   		            {
   		                /* Use video information when it has arrived */
   		                Display.setVideoList( Data.getVideoNames() );
   		                Video.updateCurrentVideo();
   		            }
   		        Server.fetchVideoList(); /* Request video information from server */

   		        // Enable key event processing
   		        this.enableKeys();

   		        widgetAPI.sendReadyEvent(); 
   				 }
   			 else
   		    {
   		       alert("Failed to initialise");
   		    }
   		 }
   		 else if(language=="Ar"){
   		alert(language);	
   		if(ServerArabe.init())
			 {
			 Display.setVolume( Audio.getVolume() );
	        Display.setTime(0);
	        
	        Player.stopCallback = function()
	        {
	            /* Return to windowed mode when video is stopped
	                (by choice or when it reaches the end) */
	        	Video.setWindowMode();
	        }

	        // Start retrieving data from server
	        ServerArabe.dataReceivedCallback = function()
	            {
	                /* Use video information when it has arrived */
	                Display.setVideoList( Data.getVideoNames() );
	                Video.updateCurrentVideo();
	            }
	        ServerArabe.fetchVideoList(); /* Request video information from server */

	        // Enable key event processing
	        this.enableKeys();

	        widgetAPI.sendReadyEvent(); 
			 }
		 else
	    {
	       alert("Failed to initialise");
	    }
   		 }
          
    }
   
}

Video.onUnload = function()
{
    Player.deinit();
}

Video.updateCurrentVideo = function(move)
{
    Player.setVideoURL( Data.getVideoURL(this.selectedVideo) );
    
    Display.setVideoListPosition(this.selectedVideo, move);

    Display.setDescription( Data.getVideoDescription(this.selectedVideo));
}

Video.enableKeys = function()
{
    document.getElementById("anchor").focus();
}

Video.keyDown = function()
{
    var keyCode = event.keyCode;
    alert("Key pressed: " + keyCode);
    
    switch(keyCode)
    {
        case tvKey.KEY_RETURN:
        case tvKey.KEY_PANEL_RETURN:
        	
            alert("RETURN");
            Player.stopVideo();
            alert("Stop Video");
            // widgetAPI.sendReturnEvent();
        		widgetAPI.blockNavigation(event);
        		parent.location.replace("menu.html"); 
            break;    
       //     break;
        case tvKey.EXIT:
			//widgetAPI.blockNavigation(event);
			widgetAPI.preventDefault(); 
		//	openExitPopUp();
			break;
        case tvKey.KEY_PLAY:
            alert("PLAY");
            
            this.handlePlayKey();
            break;
            
        case tvKey.KEY_STOP:
            alert("STOP");
            Player.stopVideo();
            break;
            
        case tvKey.KEY_PAUSE:
            alert("PAUSE");
            this.handlePauseKey();
            break;
            
        case tvKey.KEY_FF:
            alert("FF");
            if(Player.getState() != Player.PAUSED)
                Player.skipForwardVideo();
            break;
        
        case tvKey.KEY_RW:
            alert("RW");
            if(Player.getState() != Player.PAUSED)
                Player.skipBackwardVideo();
            break;

        case tvKey.KEY_VOL_UP:
        case tvKey.KEY_PANEL_VOL_UP:
            alert("VOL_UP");
            if(this.mute == 0)
                Audio.setRelativeVolume(0);
            break;
            
        case tvKey.KEY_VOL_DOWN:
        case tvKey.KEY_PANEL_VOL_DOWN:
            alert("VOL_DOWN");
            if(this.mute == 0)
                Audio.setRelativeVolume(1);
            break;      

        case tvKey.KEY_DOWN:
            alert("DOWN");
            this.selectNextVideo(this.DOWN);
            break;
            
        case tvKey.KEY_UP:
            alert("UP");
            this.selectPreviousVideo(this.UP);
            break;            

        case tvKey.KEY_ENTER:
        case tvKey.KEY_PANEL_ENTER:
            alert("ENTER");
            this.toggleMode();
            break;
        
        case tvKey.KEY_MUTE:
            alert("MUTE");
            this.muteMode();
            break;
            
        default:
            alert("Unhandled key");
            break;
    }
}

Video.handlePlayKey = function()
{
    switch ( Player.getState() )
    {
        case Player.STOPPED:
            Player.playVideo();
            break;
            
        case Player.PAUSED:
            Player.resumeVideo();
            break;
            
        default:
            alert("Ignoring play key, not in correct state");
            break;
    }
}

Video.handlePauseKey = function()
{
    switch ( Player.getState() )
    {
        case Player.PLAYING:
            Player.pauseVideo();
            break;
        
        default:
            alert("Ignoring pause key, not in correct state");
            break;
    }
}

Video.selectNextVideo = function(down)
{
    Player.stopVideo();
    
    this.selectedVideo = (this.selectedVideo + 1) % Data.getVideoCount();

    this.updateCurrentVideo(down);
}

Video.selectPreviousVideo = function(up)
{
    Player.stopVideo();
    
    if (--this.selectedVideo < 0)
    {
        this.selectedVideo += Data.getVideoCount();
    }

    this.updateCurrentVideo(up);
}

Video.setFullScreenMode = function()
{
    if (this.mode != this.FULLSCREEN)
    {
        Display.hide();
        
        Player.setFullscreen();
        
        this.mode = this.FULLSCREEN;
    }
}

Video.setWindowMode = function()
{
    if (this.mode != this.WINDOW)
    {
        Display.show();
        
        Player.setWindow();
        
        this.mode = this.WINDOW;
    }
}

Video.toggleMode = function()
{
    if(Player.getState() == Player.PAUSED)
    {
        Player.resumeVideo();
     }
    switch (this.mode)
    {
        case this.WINDOW:
            this.setFullScreenMode();
            break;
            
        case this.FULLSCREEN:
            this.setWindowMode();
            break;
            
        default:
            alert("ERROR: unexpected mode in toggleMode");
            break;
    }
}


Video.setMuteMode = function()
{
    if (this.mute != this.YMUTE)
    {
        var volumeElement = document.getElementById("volumeInfo");
        //Audio.plugin.SetSystemMute(true);
        Audio.plugin.SetUserMute(true);
        document.getElementById("volumeBar").style.backgroundImage = "url(../../images/videoBox/muteBar.png)";
        document.getElementById("volumeIcon").style.backgroundImage = "url(../../images/videoBox/mute.png)";
        widgetAPI.putInnerHTML(volumeElement, "MUTE");
        this.mute = this.YMUTE;
    }
}

Video.noMuteMode = function()
{
    if (this.mute != this.NMUTE)
    {
        Audio.plugin.SetUserMute(false); 
        document.getElementById("volumeBar").style.backgroundImage = "url(../../images/videoBox/volumeBar.png)";
        document.getElementById("volumeIcon").style.backgroundImage = "url(../../images/videoBox/volume.png)";
        Display.setVolume( Audio.getVolume() );
        this.mute = this.NMUTE;
    }
}

Video.muteMode = function()
{
    switch (this.mute)
    {
        case this.NMUTE:
            this.setMuteMode();
            break;
            
        case this.YMUTE:
            this.noMuteMode();
            break;
            
        default:
            alert("ERROR: unexpected mode in muteMode");
            break;
    }
}

window.onShow = function()
{ Connexion.init();
setInterval('Connexion.cyclicInternetConnectionCheck()', Connexion.internetConnectionInterval); 
};