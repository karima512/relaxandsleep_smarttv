var AudioMusic =
{
    plugin : null
}

AudioMusic.init = function()
{
    var success = true;
    
    this.plugin = document.getElementById("pluginAudio");
    
    if (!this.plugin)
    {
        success = false;
    }

    return success;
}

AudioMusic.setRelativeVolume = function(delta)
{
    this.plugin.SetVolumeWithKey(delta);
    DisplayMusic.setVolume( this.getVolume() );

}

AudioMusic.getVolume = function()
{
    alert("Volume : " +  this.plugin.GetVolume());
    return this.plugin.GetVolume();
}
