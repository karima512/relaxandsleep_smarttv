var widgetAPI = new Common.API.Widget();
var tvKey = new Common.API.TVKeyValue();

var current_item=1;
var total_item=10;
var direction;
var carousel;
var element;
var plugin;
var language;
var imageID;
var tipstosleep =
{
		
};

tipstosleep.onLoad = function()
{
	this.enableKeys();
	widgetAPI.sendReadyEvent();

	carousel = $("#waterwheel-carousel").waterwheelCarousel({
	    orientation: 'vertical',quickerForFurther :true,
	    separation: 300,
	    horizonOffset: 60,
	    opacityMultiplier: .9,
	    movedToCenter: function($newCenterItem) {
	    	imageID = $newCenterItem.attr('id');
	    	carouselChange(imageID);
	      }
	  });
	
	
	if(sessionStorage.getItem("language")!=null)
		language=sessionStorage.getItem("language");

	    if(language=="En"){		   
	    	document.getElementById('name').innerHTML ="KEEP A SLEEP SCHEDULE";
	    	document.getElementById('fact').innerHTML ="It's best to wake up and go to sleep at the same time every day. It's takes 7 days to adjust a new sleep pattern, but just 1 day to return to a typical sleeping pattern.";
	    }
		if(language=="Ar"){			
			image('name1.png',"name");
			image('fact1.png',"fact");
		}
		
			
};

tipstosleep.onUnload = function()
{

};

tipstosleep.enableKeys = function()
{
	document.getElementById("anchor").focus();
};

tipstosleep.keyDown = function()
{
	var keyCode = event.keyCode;
	alert("Key pressed: " + keyCode);

	switch(keyCode)
	{
		case tvKey.KEY_RETURN:
		case tvKey.KEY_PANEL_RETURN:
			widgetAPI.blockNavigation(event);
			parent.location.replace("menu.html");
			alert("RETURN");
			//widgetAPI.sendReturnEvent();
			break;
		case tvKey.KEY_INFO:
			break;
		case tvKey.KEY_LEFT:
			alert("LEFT");
			break;
		case tvKey.KEY_RIGHT:
			alert("RIGHT");
			break;
		case tvKey.KEY_UP:
			navigation("UP");
			alert("UP");
			break;
		case tvKey.KEY_DOWN:
			navigation("DOWN");
			alert("DOWN");
			break;
		case tvKey.KEY_ENTER:
		case tvKey.KEY_PANEL_ENTER:
			alert("ENTER");
			break;
		default:
			alert("Unhandled key");
			break;
	}
};


function navigation(direction){
	if(direction=="UP"){	
		carousel.prev();
	}
	if(direction=="DOWN"){
		carousel.next();
	}
}

function carouselChange(id){
	 if(language=="En"){	
		 
	if(id=="1"){
		document.getElementById('name').innerHTML ="KEEP A SLEEP SCHEDULE";
		document.getElementById('fact').innerHTML ="It's best to wake up and go to sleep at the same time every day. It's takes 7 days to adjust a new sleep pattern, but just 1 day to return to a typical sleeping pattern.";
	}
	if(id=="2")
	{
		document.getElementById('name').innerHTML ="EXERCISE REGULARLY";
		document.getElementById('fact').innerHTML ="People who exercise regularly are more likely to sleep better, and tend to get tired at an appropriate time. Even adding a 10 minute walk every day improves one's likelihood of a good night's rest.";
	}
	if(id=="3")
	{
		document.getElementById('name').innerHTML ="TAKE A HOT SHOWER";
		document.getElementById('fact').innerHTML ="A hot shower or warm bath before bed relaxes the body and makes it easier to fall asleep. When your body temperature falls your body feels more lethargic due to natural decrease in metabolic activity.";
	}
	if(id=="4")
	{
		document.getElementById('name').innerHTML ="AVOID EATING BEFORE SLEEP";
		document.getElementById('fact').innerHTML ="Because the body is digesting food, your heart rate increased from to metabolize it. It's also increases the frequency of waking up in the middle of the night.";
	}
	if(id=="5")
	{
		document.getElementById('name').innerHTML ="AVOID ALCOHOL BEFORE SLEEP";
		document.getElementById('fact').innerHTML ="Although alcohol helps you relax, it can diminish your quality of sleep by increasing the frequency of waking up during the night and lessening the time spent in REM sleep, your most restorative phase.";
	}
	if(id=="6")
	{
		document.getElementById('name').innerHTML ="AVOID CAFFEINE BEFORE SLEEP";
		document.getElementById('fact').innerHTML ="Caffeine makes us feel more alert by blocking sleep-introducing chemicals in the brain and increasing adrenaline production. Consuming any before bed will leave you tossing and turning for hours.";
	}
	if(id=="7")
	{
		document.getElementById('name').innerHTML ="AVOID LONG DAYTIME NAPS";
		document.getElementById('fact').innerHTML ="Taking longer than 20 minutes power nap will make it harder to fall asleep at night.";
	}
	if(id=="8")
	{
		document.getElementById('name').innerHTML ="READ A BOOK IN BED";
		document.getElementById('fact').innerHTML ="This may not be scientific, but reading a book always makes you tired. Always.";
	}
	if(id=="9")
	{
		document.getElementById('name').innerHTML ="KEEP IT QUIET";
		document.getElementById('fact').innerHTML ="Sleeping in a loud environment increases the likelihood of waking up at night. The quieter it is, the less distractions, there are to the body's rest.";
	}
	if(id=="10")
	{
		document.getElementById('name').innerHTML ="KEEP IT `JUST RIGHT` ";
		document.getElementById('fact').innerHTML ="The ideal bedroom temperature for sleeping is between 65 and 72 degrees. Too hot or too cold will make it harder to sleep well.";
	}
	 }
	 if(language=="Ar"){

			if(id=="1"){
				image('name1.png',"name");
				image('fact1.png',"fact");
				
			}
			if(id=="2")
			{
				image('name2.png',"name");
				image('fact2.png',"fact");
			}
			if(id=="3")
			{
				image('name3.png',"name");
				image('fact3.png',"fact");
				}
			if(id=="4")
			{
				image('name4.png',"name");
				image('fact4.png',"fact");
			}
			if(id=="5")
			{
				image('name5.png',"name");
				image('fact5.png',"fact");
			}
			if(id=="6")
			{
				image('name6.png',"name");
				image('fact6.png',"fact");
			}
			if(id=="7")
			{
				image('name7.png',"name");
				image('fact7.png',"fact");	
			}
			if(id=="8")
			{
				image('name8.png',"name");
				image('fact8.png',"fact");
			}
			if(id=="9")
			{
				image('name9.png',"name");
				image('fact9.png',"fact");
			}
			if(id=="10")
			{
				image('name10.png',"name");
				image('fact10.png',"fact");
			}

		}
	
	
}


function image(thisImg,div) {
	
	var dv = document.getElementById(div);

	// remove all child nodes
	while (dv.hasChildNodes()) { 
	    dv.removeChild(dv.lastChild); 
	} 	
    var img = document.createElement("IMG");
    img.src = "images/Localization/"+thisImg;
    document.getElementById(div).appendChild(img);
}

window.onShow = function()
{ Connexion.init();
setInterval('Connexion.cyclicInternetConnectionCheck()', Connexion.internetConnectionInterval); 
};

