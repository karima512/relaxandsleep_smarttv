var widgetAPI = new Common.API.Widget();
var tvKey = new Common.API.TVKeyValue();
var current_item=1;
var total_item=4;
var direction;
var carousel;
var imageID=1;
var element;
var language;
var pop=false;
var popExit=false;
var inf=false;
var popQuit=false;
var Menu =
{

};

Menu.onLoad = function()
{	
	this.enableKeys();
	widgetAPI.sendReadyEvent();
	if(sessionStorage.getItem("currentMenu")!=null)
		current_item=sessionStorage.getItem("currentMenu");
	
	 carousel = $("#carousel").waterwheelCarousel({
         flankingItems: 4, startingItem: current_item,
         movedToCenter: function($newCenterItem) {
        	 imageID = $newCenterItem.attr('id');
 	      }
	 });  
	 imageID=current_item;
};

Menu.onUnload = function()
{

};

Menu.enableKeys = function()
{
	document.getElementById("anchor").focus();
};

Menu.keyDown = function()
{
	var keyCode = event.keyCode;
	alert("Key pressed: " + keyCode);

	switch(keyCode)
	{
		case tvKey.KEY_RETURN:
		case tvKey.KEY_PANEL_RETURN:
			widgetAPI.blockNavigation(event);
			if(pop==true)
				closePopUp();
			else
			{parent.location.replace("index.html");
			sessionStorage.setItem("currentStart",1);}
			alert("RETURN");
			//widgetAPI.sendReturnEvent();
			break;
		case tvKey.EXIT:
			//widgetAPI.blockNavigation(event);
			widgetAPI.preventDefault(); 
			openExitPopUp();
			break;
		case tvKey.KEY_INFO:
			inf=true;
			if(popQuit==false)
				openPopUp();
			break;
		case tvKey.KEY_LEFT:
			if(pop==false)
			navigation("LEFT");
			alert("LEFT");
			break;
		case tvKey.KEY_RIGHT:
			if(pop==false)
			navigation("RIGHT");
			alert("RIGHT");
			break;
		case tvKey.KEY_UP:
			alert("UP");
			break;
		case tvKey.KEY_DOWN:
			alert("DOWN");
			break;
		case tvKey.KEY_ENTER:
		case tvKey.KEY_PANEL_ENTER:
			if(pop==false)
				changePage();
			alert("ENTER");
			break;
		default:
			alert("Unhandled key");
			break;
	}
};
function prev () {
    carousel.prev();
    return false;
  }

function next () {
    carousel.next();
    return false;
  }
function navigation(direction){
	if(direction=="LEFT"){	
		carousel.prev();
	}
	if(direction=="RIGHT"){
		carousel.next();
	}
}

function changePage(){
	alert("Path: " + parent.location)
	if(imageID==1)
		parent.location.replace("music.html");
	if(imageID==2)
		parent.location.replace("meditation.html");
	if(imageID==3)
		parent.location.replace("video.html");
	if(imageID==4)
		parent.location.replace("tipstosleep.html");
	alert("New path: " + parent.location)
	sessionStorage.setItem("currentMenu",imageID);
}

function openPopUp()
{  
	pop=true;
	 if(language=="En") {
		 document.getElementById("popup").style.backgroundImage = 'url("images/backgrounds/aboutPopup.png")';
	 }
	 if(language=="Ar"){
		 document.getElementById("popup").style.backgroundImage = 'url("images/Localization/aboutPopupAr.png")';
	 }
	element = document.getElementById("background");
	element.style.display = "block";
	element= document.getElementById("popup");
	element.style.display = "block";
}

function closePopUp()
{
	pop=false;
	element = document.getElementById("background");
	element.style.display = "none";
	element = document.getElementById("popup");
	element.style.display = "none";
}

function image(thisImg,div) {
	
	var dv = document.getElementById(div);

	// remove all child nodes
	while (dv.hasChildNodes()) { 
	    dv.removeChild(dv.lastChild); 
	} 
    var img = document.createElement("IMG");
    img.src = "images/Localization/"+thisImg;
    document.getElementById(div).appendChild(img);
}
window.onShow = function()
{ Connexion.init();
setInterval('Connexion.cyclicInternetConnectionCheck()', Connexion.internetConnectionInterval); 
};